module.exports = {
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  transform: {
    "^.+\\.vue$": "vue-jest",
  },
  moduleNameMapper: {
    ".+\\.(css|scss|css\\?module|scss\\?module)$": "identity-obj-proxy",
  },
};
