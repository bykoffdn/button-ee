// TODO: Move to the component / config ???
export const SIZE = {
  TOKEN_NAME: "size",
  CONSTANT_NAME: "SIZE",
  CLASS_PREFIX: "size",
};

export const COLOR_SCHEME = {
  TOKEN_NAME: "color",
  CONSTANT_NAME: "COLOR_SCHEME",
  CLASS_PREFIX: "colorScheme",
};

export const ROUNDING = {
  TOKEN_NAME: "radius",
  CONSTANT_NAME: "ROUNDING",
  CLASS_PREFIX: "rounding",
};

export const PADDING = {
  TOKEN_NAME: "spacing",
  CONSTANT_NAME: "PADDING",
  CLASS_PREFIX: "padding",
};

export const BORDER = {
  TOKEN_NAME: "border",
  CLASS_PREFIX: "border",
};

export const OUTLINE = {
  TOKEN_NAME: "outline",
  CLASS_PREFIX: "outline",
};

export const FONT = {
  TOKEN_NAME: "font",
};

export const DYNAMIC_CONSTANTS_FILE_PATH = "./src/constants/dynamic.ts";

export const DESIGN_DICTIONARY_CSS_VARIABLES_FILE_PATH =
  "~@darwin-studio/button-ee_style-dictionary/build/variables.css";

export const SIZE_STYLES_FILE_PATH = "./src/styles/size.css";
export const COLOR_SCHEME_STYLES_FILE_PATH = "./src/styles/color-scheme.css";
export const ROUNDING_STYLES_FILE_PATH = "./src/styles/rounding.css";
export const PADDING_STYLES_FILE_PATH = "./src/styles/padding.css";
export const OUTLINE_STYLES_FILE_PATH = "./src/styles/outline.css";
export const BORDER_STYLES_FILE_PATH = "./src/styles/border.css";
