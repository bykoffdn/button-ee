import * as chalk from "chalk";
import * as designTokens from "@darwin-studio/button-ee_style-dictionary";
import {
  FONT,
  SIZE,
  SIZE_STYLES_FILE_PATH,
  COLOR_SCHEME,
  COLOR_SCHEME_STYLES_FILE_PATH,
  ROUNDING,
  ROUNDING_STYLES_FILE_PATH,
  PADDING,
  PADDING_STYLES_FILE_PATH,
  OUTLINE,
  OUTLINE_STYLES_FILE_PATH,
  BORDER,
  BORDER_STYLES_FILE_PATH,
} from "./constants";
import {
  generateBorderClassFileStrings,
  generateColorSchemeClassFileStrings,
  generateOutlineClassFileStrings,
  generatePaddingClassFileStrings,
  generateRoundingClassFileStrings,
  generateSizeClassFileStrings,
  getNakedNames,
  getPreparedClassName,
  writeClassesToFile,
} from "./utils";
import * as console from "console";

export default (): void => {
  const colorSchemeNameList = [];

  /** generate size.css **/

  // TODO: move to utils ???
  const sizeDesignTokens = designTokens[SIZE.TOKEN_NAME];
  if (sizeDesignTokens) {
    const sizeFileClasses = []; // TODO: rename ???

    const sizeTokenVariantNameList = Object.keys(sizeDesignTokens);
    sizeTokenVariantNameList.forEach((sizeTokenVariantName) => {
      // TODO: collect size token names for complex styles usage
      const className = getPreparedClassName(
        SIZE.CLASS_PREFIX,
        sizeTokenVariantName
      );
      const customPropertyName = `--${SIZE.TOKEN_NAME}-${sizeTokenVariantName}`;
      const fontCustomPropertyName = `--${FONT.TOKEN_NAME}-${sizeTokenVariantName}`;

      sizeFileClasses.push(
        generateSizeClassFileStrings(
          className,
          customPropertyName,
          fontCustomPropertyName
        )
      );
    });

    writeClassesToFile(sizeFileClasses, SIZE_STYLES_FILE_PATH);
  } else {
    const errorMessage = "Can't get size tokens";
    // TODO: replace with logger
    console.error(chalk.red(`❌ [Button EE]: ${errorMessage}`));
    throw new Error(errorMessage);
  }

  /** generate color-scheme.css **/

  const colorSchemeDesignTokens = designTokens[COLOR_SCHEME.TOKEN_NAME];
  if (colorSchemeDesignTokens) {
    const colorSchemeFileClasses = []; // TODO: rename ???

    const colorSchemeTokenVariantNameList = Object.keys(
      colorSchemeDesignTokens
    );
    colorSchemeTokenVariantNameList.forEach((colorSchemeTokenVariantName) => {
      // TODO: relay on background suffix to define color scheme name ???
      if (colorSchemeTokenVariantName.endsWith("-background")) {
        // TODO: use const for "background"
        const colorSchemeName = colorSchemeTokenVariantName.replace(
          "-background",
          ""
        );
        colorSchemeNameList.push(colorSchemeName);

        const className = getPreparedClassName(
          COLOR_SCHEME.CLASS_PREFIX,
          colorSchemeName
        );
        const customPropertyName = `--${COLOR_SCHEME.TOKEN_NAME}-${colorSchemeName}`;

        colorSchemeFileClasses.push(
          generateColorSchemeClassFileStrings(className, customPropertyName)
        );
      }
    });

    writeClassesToFile(colorSchemeFileClasses, COLOR_SCHEME_STYLES_FILE_PATH);
  } else {
    console.warn(chalk.yellow("⚠ [Button EE]: Can't get colorScheme tokens"));
  }

  /** generate rounding.css **/

  const roundingDesignTokens = designTokens[ROUNDING.TOKEN_NAME];
  if (roundingDesignTokens) {
    const roundingFileClasses = []; // TODO: rename ???

    const roundingTokenVariantNameList = Object.keys(roundingDesignTokens);
    roundingTokenVariantNameList.forEach((roundingTokenVariantName) => {
      const className = getPreparedClassName(
        ROUNDING.CLASS_PREFIX,
        roundingTokenVariantName
      );
      const customPropertyName = `--${ROUNDING.TOKEN_NAME}-${roundingTokenVariantName}`;

      roundingFileClasses.push(
        generateRoundingClassFileStrings(className, customPropertyName)
      );
    });

    writeClassesToFile(roundingFileClasses, ROUNDING_STYLES_FILE_PATH);
  } else {
    console.warn(chalk.yellow("⚠ [Button EE]: Can't get rounding tokens"));
  }

  /** generate padding.css **/

  const paddingDesignTokens = designTokens[PADDING.TOKEN_NAME];
  if (paddingDesignTokens) {
    const paddingFileClasses = []; // TODO: rename ???

    const paddingTokenVariantNameList = Object.keys(paddingDesignTokens);
    paddingTokenVariantNameList.forEach((paddingTokenVariantName) => {
      const className = getPreparedClassName(
        PADDING.CLASS_PREFIX,
        paddingTokenVariantName
      );
      const customPropertyName = `--${PADDING.TOKEN_NAME}-${paddingTokenVariantName}`;

      paddingFileClasses.push(
        generatePaddingClassFileStrings(className, customPropertyName)
      );
    });

    writeClassesToFile(paddingFileClasses, PADDING_STYLES_FILE_PATH);
  } else {
    console.warn(chalk.yellow("⚠ [Button EE]: Can't get padding tokens"));
  }

  /** generate border.css **/

  const borderDesignTokens = designTokens[BORDER.TOKEN_NAME];
  if (borderDesignTokens) {
    const borderFileClasses = []; // TODO: rename ???

    const borderTokenVariantNameList = Object.keys(borderDesignTokens);
    borderTokenVariantNameList.forEach((borderTokenVariantName) => {
      const className = getPreparedClassName(
        BORDER.CLASS_PREFIX,
        borderTokenVariantName
      );

      console.log(className);

      const sizeCustomPropertyName = `--${BORDER.TOKEN_NAME}-${borderTokenVariantName}`;

      const { nakedPrefixOrSuffixName } = getNakedNames(
        borderTokenVariantName,
        colorSchemeNameList
      );
      const colorCustomPropertyName = `--color-${nakedPrefixOrSuffixName}-${BORDER.TOKEN_NAME}`;

      // TODO: how to combine :hover,:active,:disabled for different sizes ???
      borderFileClasses.push(
        generateBorderClassFileStrings(
          className,
          sizeCustomPropertyName,
          colorCustomPropertyName
        )
      );
    });

    writeClassesToFile(borderFileClasses, BORDER_STYLES_FILE_PATH);
  } else {
    console.warn(chalk.yellow("⚠ [Button EE]: Can't get border tokens"));
  }

  /** generate outline.css **/

  const outlineDesignTokens = designTokens[OUTLINE.TOKEN_NAME];
  if (outlineDesignTokens) {
    const outlineFileClasses = []; // TODO: rename ???

    const outlineTokenVariantNameList = Object.keys(outlineDesignTokens);
    outlineTokenVariantNameList.forEach((outlineTokenVariantName) => {
      const className = getPreparedClassName(
        OUTLINE.CLASS_PREFIX,
        outlineTokenVariantName
      );

      const sizeCustomPropertyName = `--${OUTLINE.TOKEN_NAME}-${outlineTokenVariantName}`;

      const { nakedPrefixOrSuffixName } = getNakedNames(
        outlineTokenVariantName,
        colorSchemeNameList
      );
      const colorCustomPropertyName = `--color-${nakedPrefixOrSuffixName}-${OUTLINE.TOKEN_NAME}`;

      outlineFileClasses.push(
        generateOutlineClassFileStrings(
          className,
          sizeCustomPropertyName,
          colorCustomPropertyName
        )
      );
    });

    writeClassesToFile(outlineFileClasses, OUTLINE_STYLES_FILE_PATH);
  } else {
    console.warn(chalk.yellow("⚠ [Button EE]: Can't get outline tokens"));
  }

  // TODO: replace with logger
  console.info(chalk.greenBright("✔︎[Button EE]: Styles generated"));
};
