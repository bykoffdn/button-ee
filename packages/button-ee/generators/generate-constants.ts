import * as chalk from "chalk";
import * as fs from "fs";
import * as designTokens from "@darwin-studio/button-ee_style-dictionary";
import {
  SIZE,
  COLOR_SCHEME,
  ROUNDING,
  PADDING,
  DYNAMIC_CONSTANTS_FILE_PATH,
} from "./constants";
import { getNakedNames, prepareConstantString } from "./utils";

export default (): void => {
  const fileStream = fs.createWriteStream(DYNAMIC_CONSTANTS_FILE_PATH);
  const designTokenTypes = Object.keys(designTokens);

  fileStream.on("error", (error) => {
    // TODO: replace with logger
    console.error(chalk.red(error.message));
  });

  /* TODO replace with 2 steps: data collection, data write */
  // TODO: add typing ???
  designTokenTypes.forEach((tokenType, tokenIndex) => {
    const tokenVariants = designTokens[tokenType];
    const constantStrings = []; // TODO: choose more accurate name

    switch (tokenType) {
      // TODO: move to config rules how to generate tokenType
      case SIZE.TOKEN_NAME:
        SIZE.VALUES.length = 0;
        constantStrings.push(`export const ${SIZE.CONSTANT_NAME} = {`);
        Object.keys(tokenVariants).forEach((tokenVariantName) => {
          constantStrings.push(prepareConstantString(tokenVariantName));
          SIZE.VALUES.push(tokenVariantName);
        });
        break;
      case COLOR_SCHEME.TOKEN_NAME:
        constantStrings.push(`export const ${COLOR_SCHEME.CONSTANT_NAME} = {`);
        Object.keys(tokenVariants).forEach((tokenVariantName) => {
          // TODO: relay on background suffix to define color scheme name ???
          if (tokenVariantName.endsWith("-background")) {
            // TODO: use const for "background"
            constantStrings.push(
              prepareConstantString(tokenVariantName.replace("-background", ""))
            );
          }
        });
        break;
      case ROUNDING.TOKEN_NAME:
        constantStrings.push(`export const ${ROUNDING.CONSTANT_NAME} = {`);
        Object.keys(tokenVariants).forEach((tokenVariantName) => {
          constantStrings.push(prepareConstantString(tokenVariantName));
        });
        break;
      case PADDING.TOKEN_NAME:
        constantStrings.push(`export const ${PADDING.CONSTANT_NAME} = {`);
        Object.keys(tokenVariants).forEach((tokenVariantName) => {
          const { nakedVariantName } = getNakedNames(
            tokenVariantName,
            SIZE.VALUES
          );

          const preparedString = prepareConstantString(nakedVariantName);
          if (!constantStrings.find((str) => str === preparedString)) {
            constantStrings.push(prepareConstantString(nakedVariantName));
          }
        });

        break;
      default:
    }

    constantStrings.push("} as const;"); //

    if (constantStrings.length > 2) {
      fileStream.on("open", () => {
        if (tokenIndex > 0) {
          fileStream.write("\n");
        }

        constantStrings.forEach((str) => {
          fileStream.write(`${str}\n`);
        });

        const isLastToken = tokenIndex >= designTokenTypes.length - 1;
        if (isLastToken) {
          fileStream.end();
        }
      });
    }
  });

  // TODO: replace with logger
  console.info(chalk.greenBright("✔︎[Button EE]: Constants generated"));
};
