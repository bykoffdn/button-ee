// TODO: name
import { camelCase } from "camel-case";
import * as fs from "fs";
import { DESIGN_DICTIONARY_CSS_VARIABLES_FILE_PATH } from "./constants";
import * as chalk from "chalk";

export function prepareConstantString(constantVariantName: string): string {
  const hasProhibitedChars =
    constantVariantName.includes("-") || constantVariantName.includes(" "); // TODO: do it more accurate

  const constantVariantKey = hasProhibitedChars
    ? `"${constantVariantName}"`
    : constantVariantName;

  return `  ${constantVariantKey}: "${constantVariantName}",`;
}

/**
 * Clean up constantVariantName from prefixes or suffixes
 * @param constantVariantName
 * @param prefixOrPostfixList
 */
export function getNakedNames(
  constantVariantName: string,
  prefixOrPostfixList: string[]
): { nakedVariantName: string; nakedPrefixOrSuffixName: string } {
  let nakedVariantName = constantVariantName;
  let nakedPrefixOrSuffixName = "";

  prefixOrPostfixList.some((prefixOrSuffix) => {
    if (constantVariantName.startsWith(prefixOrSuffix)) {
      nakedVariantName = constantVariantName.replace(`${prefixOrSuffix}-`, "");
      nakedPrefixOrSuffixName = prefixOrSuffix;
      return true;
    }
    if (constantVariantName.endsWith(prefixOrSuffix)) {
      nakedVariantName = constantVariantName.replace(`-${prefixOrSuffix}`, "");
      nakedPrefixOrSuffixName = prefixOrSuffix;
      return true;
    }
    return false;
  });

  return { nakedVariantName, nakedPrefixOrSuffixName };
}

/**
 * Capitalize the first letter of the str
 * @param str
 */
export function capitalizeFirstLetter(str: string): string {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

/**
 * Prepare class name
 * @param tokenTypeName
 * @param tokenVariantName
 */
export function getPreparedClassName(
  tokenTypeName: string,
  tokenVariantName: string
): string | null {
  if (!tokenTypeName || !tokenVariantName) {
    return null;
  }

  return `${tokenTypeName}${capitalizeFirstLetter(
    camelCase(tokenVariantName)
  )}`;
}

export function writeClassesToFile(
  fileClassStrings: string[],
  filePath: string
): void {
  if (fileClassStrings.length) {
    const sizeFileStream = fs.createWriteStream(filePath);
    sizeFileStream.on("open", () => {
      sizeFileStream.write(
        `@import '${DESIGN_DICTIONARY_CSS_VARIABLES_FILE_PATH}';\n`
      );

      fileClassStrings.forEach((classStrings, classIndex) => {
        sizeFileStream.write(`\n${classStrings}\n`);

        if (classIndex >= fileClassStrings.length - 1) {
          sizeFileStream.end();
        }
      });
    });

    sizeFileStream.on("error", (error) => {
      // TODO: replace with logger
      console.error(chalk.red(error.message));
    });
  }
}

// TODO: naming ???
export function generateSizeClassFileStrings(
  className: string,
  sizeCustomPropertyName: string,
  fontCustomPropertyName: string
): string {
  return `.${className} {
  min-height: var(${sizeCustomPropertyName});
  min-width: var(${sizeCustomPropertyName});
  font: var(${fontCustomPropertyName});
}`;
}

// TODO: naming ???
export function generateColorSchemeClassFileStrings(
  className: string,
  customPropertyName: string
): string {
  return `.${className} {
  color: var(${customPropertyName});
  background-color: var(${customPropertyName}-background);
}

.${className}:hover {
  color: var(${customPropertyName}-hover);
  background-color: var(${customPropertyName}-background-hover);
}

.${className}:active {
  color: var(${customPropertyName}-active);
  background-color: var(${customPropertyName}-background-active);
}

.${className}:disabled {
  color: var(${customPropertyName}-disabled);
  background-color: var(${customPropertyName}-background-disabled);
}`;
}

// TODO: naming ???
export function generateRoundingClassFileStrings(
  className: string,
  customPropertyName: string
): string {
  return `.${className} {
  border-radius: var(${customPropertyName});
}`;
}

// TODO: naming ???
export function generatePaddingClassFileStrings(
  className: string,
  customPropertyName: string
): string {
  return `.${className} {
  padding: var(${customPropertyName});
}`;
}

// TODO: naming ???
export function generateOutlineClassFileStrings(
  className: string,
  sizeCustomPropertyName: string,
  colorCustomPropertyName: string
): string {
  return `.${className}:focus-visible,
.${className}[data-focus-visible-added] {
  outline: var(${sizeCustomPropertyName}) var(${colorCustomPropertyName});
}`;
}

// TODO: naming ???
export function generateBorderClassFileStrings(
  className: string,
  sizeCustomPropertyName: string,
  colorCustomPropertyName: string
): string {
  // TODO: how to combine :hover,:active,:disabled for different sizes ???
  return `.${className} {
  border: var(${sizeCustomPropertyName}) var(${colorCustomPropertyName});
}

.${className}:hover {
  border-color: var(${colorCustomPropertyName}-hover);
}

.${className}:active {
  border-color: var(${colorCustomPropertyName}-active);
}

.${className}:disabled {
  border-color: var(${colorCustomPropertyName}-disabled);
}`;
}
