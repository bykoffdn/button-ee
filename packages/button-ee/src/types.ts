import {
  COLOR_SCHEME,
  SIZE,
  ROUNDING,
  PADDING,
  ELEMENT,
} from "./constants/index";

export type Size = keyof typeof SIZE;
export type ColorScheme = keyof typeof COLOR_SCHEME;
export type Rounding = keyof typeof ROUNDING;
export type Padding = keyof typeof PADDING;
export type Element = keyof typeof ELEMENT;

export interface ButtonEEProps {
  content: string;
  type: ColorScheme;
  size: Size;
  rounding: Rounding;
  padding: Padding;
  disabled: boolean;
  href: string;
}

export interface ElementAttrs {
  class: string[];
  onClick: () => void;
  "v-html"?: string;
}
