export function isExist(value: unknown): boolean {
  return !["undefined", "null"].includes(typeof value);
}
