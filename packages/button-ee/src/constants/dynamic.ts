export const SIZE = {
  tiny: "tiny",
  small: "small",
  medium: "medium",
  large: "large",
  huge: "huge",
} as const;

export const PADDING = {
  none: "none",
  equal: "equal",
  default: "default",
} as const;

export const ROUNDING = {
  none: "none",
  small: "small",
  medium: "medium",
  large: "large",
  full: "full",
} as const;

export const COLOR_SCHEME = {
  primary: "primary",
  secondary: "secondary",
  alternative: "alternative",
  inverse: "inverse",
  danger: "danger",
} as const;
