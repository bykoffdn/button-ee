import "focus-visible/dist/focus-visible.min.js";
import { defineComponent, PropType, VNode, capitalize } from "vue";

import {
  COLOR_SCHEME,
  SIZE,
  ROUNDING,
  ELEMENT,
  PADDING,
} from "./constants/index";
import {
  ColorScheme,
  Size,
  Rounding,
  Element,
  Padding,
  ElementAttrs,
} from "./types";

import stylesCommon from "./styles/common.css?module";
import stylesColorScheme from "./styles/color-scheme.css?module";
import stylesSize from "./styles/size.css?module";
import stylesRounding from "./styles/rounding.css?module";
import stylesPadding from "./styles/padding.css?module";
import stylesBorder from "./styles/border.css?module";
import stylesOutline from "./styles/outline.css?module";

export default defineComponent({
  name: "ButtonEE",

  emits: ["click"],

  props: {
    /*TODO: label, text, move name to a const ???*/
    content: {
      type: String,
    },

    /**
     * TODO: Add description
     */
    colorScheme: {
      type: String as PropType<ColorScheme>,
      default: COLOR_SCHEME.primary,
    },

    size: {
      type: String as PropType<Size>,
      default: SIZE.medium,
    },

    rounding: {
      type: String as PropType<Rounding>,
      default: ROUNDING.medium,
    },

    padding: {
      type: String as PropType<Padding>,
      default: PADDING.default,
    },

    whenClick: {
      type: Function as PropType<() => void>,
    },
  },

  computed: {
    // TODO: more accurate typing ???
    classes(): string[] {
      /*TODO: use propName const ???*/
      const size = capitalize(this.size);
      const colorScheme = `${capitalize(this.colorScheme)}`;
      const colorSchemeSize = `${colorScheme}${size}`;

      const sizeKey = `size${size}`;
      const colorSchemeKey = `colorScheme${colorScheme}`;
      const roundingKey = `rounding${capitalize(this.rounding)}`;
      // size or colorScheme dependent
      const paddingKey = `padding${capitalize(this.padding)}${size}`;
      const borderKey = `border${colorSchemeSize}`;
      const outlineKey = `outline${colorSchemeSize}`;

      return [
        stylesCommon.button,
        stylesColorScheme[colorSchemeKey],
        stylesSize[sizeKey],
        stylesRounding[roundingKey],
        stylesPadding[paddingKey],
        stylesBorder[borderKey],
        stylesOutline[outlineKey],
      ];
    },
  },

  methods: {
    clickHandler() {
      /**
       * Just emits click event without any payload.
       *
       * @event click
       * @type {undefined}
       */
      this.$emit("click");
      this.whenClick && this.whenClick();
    },
  },

  render(): VNode {
    let element: Element = ELEMENT.button;
    if (this.$attrs.to) {
      element = ELEMENT["router-link"];
    } else if (this.$attrs.href) {
      element = ELEMENT.a;
    }

    const attrs: ElementAttrs = {
      class: this.classes,
      onClick: this.clickHandler,
    };

    if (this.content) {
      return <element {...attrs} v-html={this.content} />;
    }

    return (
      <element {...attrs}>
        {this.$slots.default && this.$slots.default()}
      </element>
    );
  },
});
