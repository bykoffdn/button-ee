import { defineComponent, VNode } from "vue";
import styles from "./index.css?module";

export default defineComponent({
  name: "Test",

  props: {
    msg: {
      type: String,
      default: "",
    },
  },

  render(): VNode {
    return <div class={styles.test}>{this.msg}</div>;
  },
});
