import { shallowMount } from "@vue/test-utils";
import TestComponent from "@/__tests__/dumb";

describe("test.tsx", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(TestComponent, {
      props: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
