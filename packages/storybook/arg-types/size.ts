import { SIZE } from "@darwin-studio/button-ee/src/constants";

const size = {
  control: { type: "select" },
  options: Object.keys(SIZE),
};

export default size;
