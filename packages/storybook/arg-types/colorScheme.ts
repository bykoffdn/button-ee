import { COLOR_SCHEME } from "@darwin-studio/button-ee/src/constants";

const colorScheme = {
  control: { type: "select" },
  options: Object.keys(COLOR_SCHEME),
};

export default colorScheme;
