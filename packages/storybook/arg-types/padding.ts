import { PADDING } from "@darwin-studio/button-ee/src/constants";

const padding = {
  control: { type: "select" },
  options: Object.keys(PADDING),
};

export default padding;
