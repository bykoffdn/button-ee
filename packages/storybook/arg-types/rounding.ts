import { ROUNDING } from "@darwin-studio/button-ee/src/constants";

const rounding = {
  control: { type: "select" },
  options: Object.keys(ROUNDING),
};

export default rounding;
