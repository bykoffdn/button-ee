import { Story } from "@storybook/vue3";

/*TODO: use dependent on #yarn build storybook or #yarn storybook*/
/** PROD */
// import ButtonEE from "@darwin-studio/button-ee";
// import "@darwin-studio/button-ee/dist/button-ee.css";
/** DEV */
import ButtonEE from "@darwin-studio/button-ee/src";

import {
  COLOR_SCHEME,
  SIZE,
  ROUNDING,
  PADDING,
} from "@darwin-studio/button-ee/src/constants";
import { ButtonEEProps } from "@darwin-studio/button-ee/src/types";

import colorScheme from "../arg-types/colorScheme";
import size from "../arg-types/size";
import rounding from "../arg-types/rounding";
import padding from "../arg-types/padding";

const content = "Button <b>EE</b></br>🙈";

export default {
  title: "ButtonEE",
  component: ButtonEE,
  argTypes: {
    colorScheme: colorScheme,
    size,
    rounding,
    padding,
    onClick: { action: "clicked" },
  },
  args: {
    colorScheme: COLOR_SCHEME.primary,
    size: SIZE.medium,
    rounding: ROUNDING.medium,
    padding: PADDING.default,
    disabled: false,
    href: "",
  },
};

const DefaultTemplate: Story<ButtonEEProps> = (args: ButtonEEProps) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { ButtonEE },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <ButtonEE v-bind="args" />
  `,
});

const SlotTemplate: Story<ButtonEEProps> = (args: ButtonEEProps) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { ButtonEE },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <ButtonEE v-bind="args"><i>Some slot content</i></ButtonEE>
  `,
});

export const UsingContentProp = DefaultTemplate.bind({});
UsingContentProp.args = { content };

export const UsingSlot = SlotTemplate.bind({});

// TODO: add multiple stories
